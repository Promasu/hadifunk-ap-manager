import re


def generate_rc_local(allowed_macs):
    config = ""
    for mac in allowed_macs:
        if re.match("^([0-9A-Fa-f]{2}:){5}([0-9A-Fa-f]{2})$", mac):
            config += "ebtables -A FORWARD -i eth0.4 -s '{}' -j ACCEPT\n".format(mac)
        else:
            print("[1] MAC: `" + mac + "` is not valid!")
    return config + "ebtables -A FORWARD -i eth0.4 -j DROP"


def generate_config_network(device_mac, device_ipv4):
    with open('defaults/config_network') as f:
        config_template = f.read()
        return config_template.replace('$device_mac$', device_mac).replace('$device_ipv4$', device_ipv4)


def generate_config_snmpd(device_name):
    with open('defaults/config_snmpd') as f:
        config_template = f.read()
        return config_template.replace('$device_name$', device_name)


def generate_config_system(device_name):
    with open('defaults/config_system') as f:
        config_template = f.read()
        return config_template.replace('$device_name$', device_name)


def generate_config_wireless(ssid, radius_server, radius_port, radius_secret):
    with open('defaults/config_wireless') as f:
        config_template = f.read()
        return config_template.replace('$ssid$', ssid).replace('$radius_server$', radius_server)\
            .replace('$radius_port$', radius_port).replace('$radius_secret$', radius_secret)


def generate_dropbear_authorized_keys(authorized_keys):
    return "\n".join(authorized_keys)
