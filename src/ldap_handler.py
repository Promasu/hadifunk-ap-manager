import ldapom


class LDAPHandler:
    def __init__(self, uri, base, dn, password):
        self.ldap = ldapom.LDAPConnection(uri, base, dn, password)

    def get_node(self, dn):
        entry = self.ldap.get_entry(dn)
        entry.fetch()
        return entry
        #return [entry for entry in [self.ldap.get_entry(dn)] if entry.fetch() or True][0] #tjerimagic@irc.hadiko.de

    def get_children(self, search_base, object_class):
        s = self.ldap.search("(|(objectClass={}))".format(object_class), base=search_base, scope=1)
        return [entry for entry in s if entry.fetch() or True]

    def plain_search(self, search_base, searchstring):
        s = self.ldap.search(searchstring, base=search_base, scope=1)
        return [entry for entry in s if entry.fetch() or True]
