def get_aps(ldap):
    return ldap.get_children("ou=wifi,ou=system,dc=hadiko,dc=de", "accessPoint")


def get_openwrt_aps(ldap):
    return ldap.get_children("ou=wifi,ou=system,dc=hadiko,dc=de", "openWrtAccessPoint")


def get_unifi_aps(ldap):
    return ldap.get_children("ou=wifi,ou=system,dc=hadiko,dc=de", "unifiAccessPoint")


def get_ap(ldap, ap_name):
    return ldap.get_node("cn={},ou=wifi,ou=system,dc=hadiko,dc=de".format(ap_name))
