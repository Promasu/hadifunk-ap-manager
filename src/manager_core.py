import ldap_handler
import openwrt_config_builder as cb
import local_settings as ls
import os


class ManagerCore:
    def __init__(self):
        self.ldap = ldap_handler.LDAPHandler(ls.LDAP_URI, ls.LDAP_BASE_DN, ls.LDAP_BIND_DN, ls.LDAP_BIND_PASSWORD)

    def handle_single_device_action(self, device_name, action):
        if action is "ssh_keys":
            self.push_authorized_keys(device_name)
        elif action is "allowed_macs":
            self.push_allowed_macs(device_name)
        elif action is "radius_secret":
            self.push_new_radius_secret(device_name)
        elif action is "openwrt_update":
            self.install_openwrt_update(device_name)
        else:
            print("Action is unknown.")

    def handle_test_device_action(self, action):
        aps = self.ldap.plain_search(ls.LDAP_DEVICE_BASE, "(|(isTestAp=TRUE))")
        for ap in aps:
            self.handle_single_device_action(ap.deviceName, action)

    def handle_prod_device_action(self, action):
        aps = self.ldap.plain_search(ls.LDAP_DEVICE_BASE, "(|(isTestAp=FALSE))")
        for ap in aps:
            self.handle_single_device_action(ap.deviceName, action)

    def handle_all_device_action(self, action):
        aps = self.ldap.get_children(ls.LDAP_DEVICE_BASE, "accessPoint")
        for ap in aps:
            self.handle_single_device_action(ap.deviceName, action)

    def push_allowed_macs(self, device_name):
        device = self.ldap.plain_search("ou=wifi,ou=system,dc=hadiko,dc=de", "(|(deviceName={}))".format(device_name))
        if len(device) > 1:
            print("[2] Multiple devices with the name `{}` exist. Exiting...".format(device_name))
            return False
        else:
            mac_config = cb.generate_rc_local(device[0].allowedMacs)
            # TODO: Push config to device

    def push_authorized_keys(self, device_name):
        self.get_ssh_keys()
        pass

    def push_new_radius_secret(self, device_name):
        device = self.ldap.plain_search("ou=wifi,ou=system,dc=hadiko,dc=de", "(|(deviceName={}))".format(device_name))
        if len(device) > 1:
            print("[2] Multiple devices with the name `{}` exist. Exiting...".format(device_name))
            return False
        else:
            if device.isTestAP:
                ssid = ls.WIFI_TEST_SSID
            else:
                ssid = ls.WIFI_PROD_SSID
            wireless_config = cb.generate_config_wireless(ssid, ls.RADIUS_SERVER, ls.RADIUS_PORT, device.radiusSecret)
        #TODO: Push config to device

    def install_openwrt_update(self, device_name):
        #TODO: Evaluate ways to update
        pass

    def install_new_device(self, device_name):
        device = self.ldap.plain_search("ou=wifi,ou=system,dc=hadiko,dc=de", "(|(deviceName={}))".format(device_name))
        if len(device) > 1:
            print("[2] Multiple devices with the name `{}` exist. Exiting...".format(device_name))
            return False
        else:
            wifi_config = cb.generate_config_network(device[0].deviceMacAddress, device[0].deviceIpv4Address)
            #TODO: Don't know how to handle because of the manual steps needed

    def get_ssh_keys(self):
        wifi_users = self.ldap.get_node("cn=sys-wlan,ou=server,dc=hadiko,dc=de").uniqueMember
        sudo_users = self.ldap.get_node("cn=sys-jedi,ou=server,dc=hadiko,dc=de").uniqueMember
        authorized_users = wifi_users.union(sudo_users)
        return "\n".join(key.sSHKeyFile for key in [self.ldap.get_children(str(user), "sSHKey")for user
                                                    in authorized_users] if key.sSHKeyDevice in ["*", "radius"])
